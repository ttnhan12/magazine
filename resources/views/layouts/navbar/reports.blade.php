<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="far fa-chart-bar"></i> Reports <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{route('report.contributions')}}">
            <i class="far fa-chart-bar"></i> Number of Contributions
        </a>
        <a class="dropdown-item" href="{{route('report.percentage')}}">
            <i class="far fa-chart-bar"></i> Percentage of Contributions
        </a>
        <a class="dropdown-item" href="{{route('report.contributors')}}">
            <i class="far fa-chart-bar"></i> Number of Contributors
        </a>
        <a class="dropdown-item" href="{{route('report.withoutConment')}}">
            <i class="far fa-chart-bar"></i> Contributions without a comment
        </a>
        <a class="dropdown-item" href="{{route('report.afterFourteen')}}">
            <i class="far fa-chart-bar"></i> Contributions without a comment after 14 days
        </a>
    </div>
</li>