{{-- @if (session()->has('status'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('status') }}
        </div>
@endif --}}

@if (session()->has('status'))
    <div class="alert alert-dismissable alert-success fade show">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {{ session()->get('status') }}
        </strong>
    </div>
@endif