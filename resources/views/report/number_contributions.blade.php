@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="chart-container">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <script>
        var chartColors = {
          red: 'rgb(255, 99, 132)',
          orange: 'rgb(255, 159, 64)',
          yellow: 'rgb(255, 205, 86)',
          green: 'rgb(75, 192, 192)',
          blue: 'rgb(54, 162, 235)',
          purple: 'rgb(153, 102, 255)',
          grey: 'rgb(201, 203, 207)'
        };
		var ds = [{
			data:[
				[1,2,3,4],
				[1,2,3,4],
				[1,2,3,4],
				[1,2,3,4]
			],
			backgroundColor:[
				chartColors.blue,
                chartColors.red,
                chartColors.yellow,
                chartColors.green,
                chartColors.purple,
                chartColors.grey,
                chartColors.orange
			],
			labels:['Philosophy', 'History', 'Math', 'Physics']
		}]
        var barChartData = {
			labels: ['2016', '2017', '2018', '2019'], //this is academic year
			datasets: [
			{
				label: 'Philosophy', //this is the name of faculty
				backgroundColor: chartColors.red,
				data: [1,2,3,4] //this is the total contribution corresponding to those years
			}, {
				label: 'History',
				backgroundColor: chartColors.blue,
				data: [1,2,3,4]
			}, {
				label: 'Mathematics',
				backgroundColor: chartColors.green,
                data: [1,2,3,4]
			}, {
				label: 'Information Technology',
				backgroundColor: chartColors.orange,
                data: [1,2,3,4]
			}]

		};
        var config = {
            type: 'bar',
			data: {
				labels: {{$list}}, //this is academic year
				datasets: [
						{
							label: 'Philosophy', //this is the name of faculty
							backgroundColor: chartColors.red,
							data: {{$ph}} //this is the total contribution corresponding to those years
						}, {
							label: 'History',
							backgroundColor: chartColors.blue,
							data: {{$hi}}
						}, {
							label: 'Mathematics',
							backgroundColor: chartColors.green,
							data: {{$ma}}
						}, {
							label: 'Information Technology',
							backgroundColor: chartColors.orange,
							data: {{$it}}
						}
				]
			},
			options: {
				title: {
					display: true,
					text: 'Number of contributions within each Faculty for each academic year'
				},
				tooltips: {
					mode: 'index',
					intersect: false
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true
					}]
				}
			}
        };
        window.onload = function() {
          var ctx = document.getElementById('myChart').getContext('2d');
          var myChart = new Chart(ctx, config);
        };
      </script>
</div>
@endsection
