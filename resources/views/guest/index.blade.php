@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table table-bordered table-hover">
        <caption>List of selected contributions - {{Auth::user()->faculty->name}}</caption>
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Preview</th>
                <th scope="col">Student name</th>
                <th scope="col">File type</th>
                <th scope="col">Description</th>
                <th scope="col">Date</th>
            </tr>
        </thead>
        <tbody>
            @forelse($contributions as $contribution)
            <tr>
                <th scope="row">{{$loop->index + 1}}</th>
                <td>
                    @if($contribution->file_extension != 'docx')
                        <img width="50" src="{{asset('storage/'.$contribution->file_path)}}" alt="preview">
                    @else
                        <a class="text-secondary" href="{{asset('storage/'.$contribution->file_path)}}">Download file</a>
                    @endif
                </td>
                <td>{{$contribution->user->name}}</td>
                <td>
                    @if($contribution->file_extension != 'docx')
                        Image
                    @else
                        Word document
                    @endif
                </td>
                <td>
                    @if($contribution->description)
                        {{$contribution->description}}
                    @else
                        <small class="font-italic">no description</small>
                    @endif
                </td>
                <td>{{$contribution->created_at}}</td>
            </tr>
            @empty
                <td colspan="6" class="text-center">No records</td>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
