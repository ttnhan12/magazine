@extends('layouts.app')

@section('content')
<div class="container">
    <h4>Create new record</h4>
    @include('layouts.success')
    @include('layouts.errors')
    <div class="card w-50">
        <div class="card-body">
            <form action="{{route('admin.closure.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label>Closure Date</label>
                    <input type="date" name="closure_date" class="form-control">
                </div>
                <div class="form-group">
                    <label>Final Closure Date</label>
                    <input type="date" name="final_closure_date" class="form-control">
                </div>
                <button class="btn btn-primary" type="submit">Create</button>
            </form>
        </div>
    </div>
</div>
@endsection
