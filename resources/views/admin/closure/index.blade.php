@extends('layouts.app')

@section('content')
<div class="container">
    <h4>Manage Closure Dates</h4>
    @include('layouts.success')
    @include('layouts.errors')
    <a href="{{route('admin.closure.create')}}">Create new record</a>
    <table class="table table-bordered table-hover">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Closure Date</th>
                <th scope="col">Final Closure Date</th>
                <th scope="col">Academic Year</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($closures as $closure)
            <tr>
                <th scope="row">{{$loop->index + 1}}</th>
                <td>{{$closure->closure_date}}</td>
                <td>{{$closure->final_closure_date}}</td>
                <td>{{$closure->academic_year}}</td>
                <td><a href="{{route('admin.closure.edit',['id'=>$closure->id])}}">Edit</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
