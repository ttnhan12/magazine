create file .env then copy all contents for file .env.example

config database information

composer install

php artisan key:generate

php artisan migrate:refresh --seed

php artisan storage:link