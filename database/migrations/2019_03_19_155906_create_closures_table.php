<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosuresTable extends Migration
{
    //constrain
    //final date must be greater than closure date
    //final date and closure date must be in the same year
    //each record is unique in term of year
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('closure_date')->nullable();
            $table->date('final_closure_date')->nullable();
            $table->year('academic_year')->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closures');
    }
}
