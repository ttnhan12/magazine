<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClosuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('closures')->insert([
            [
                'closure_date'=> Carbon::now('Asia/Ho_Chi_Minh'),
                'final_closure_date'=> Carbon::now('Asia/Ho_Chi_Minh'),
                'academic_year' => Carbon::now('Asia/Ho_Chi_Minh')->year,
                'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),
                'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')
            ],
        ]);
    }
}
