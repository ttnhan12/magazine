<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FacultiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            ['name'=>'Faculty of Philosophy','created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Faculty of Mathematics','created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Faculty of Information Technology','created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Faculty of History','created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')]
        ]);
    }
}
