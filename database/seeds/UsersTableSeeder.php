<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Administrator
        DB::table('users')->insert([
            ['name'=>'Administrator','email'=>'admin@gmail.com','password'=>bcrypt('password'),'user_type'=>0,'faculty_id'=>0,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
        ]);
        //Marketing Manager
        DB::table('users')->insert([
            ['name'=>'Marketing Manager','email'=>'manager@gmail.com','password'=>bcrypt('password'),'user_type'=>1,'faculty_id'=>0,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
        ]);
        //Marketing Coordinator
        DB::table('users')->insert([
            ['name'=>'Philosophy Faculty','email'=>'mcphilosophy@gmail.com','password'=>bcrypt('password'),'user_type'=>2,'faculty_id'=>1,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Mathematics Faculty','email'=>'mcmath@gmail.com','password'=>bcrypt('password'),'user_type'=>2,'faculty_id'=>2,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Information Technology Faculty','email'=>'mcit@gmail.com','password'=>bcrypt('password'),'user_type'=>2,'faculty_id'=>3,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'History Faculty','email'=>'mchistory@gmail.com','password'=>bcrypt('password'),'user_type'=>2,'faculty_id'=>4,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
        ]);
        //Student
        DB::table('users')->insert([
            ['name'=>'Jessica K. Cintron','email'=>'cintron@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>1,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Rima J. Pack','email'=>'pack@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>1,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Faye D. May','email'=>'may@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>1,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Amy J. Payton','email'=>'payton@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>2,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Helen F. Mobley','email'=>'mobley@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>2,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Charles N. Smith','email'=>'smith@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>2,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Rory L. Lara','email'=>'lara@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>3,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'James C. Gomez','email'=>'gomez@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>3,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'William D. Ladouceur','email'=>'ladouceur@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>3,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Margaret S. Jones','email'=>'jones@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>4,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Richard J. Moses','email'=>'moses@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>4,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Marion W. Cameron','email'=>'cameron@gmail.com','password'=>bcrypt('password'),'user_type'=>3,'faculty_id'=>4,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
        ]);
        //Guest
        DB::table('users')->insert([
            ['name'=>'Guest_Philosophy','email'=>'guest_philosophy@gmail.com','password'=>bcrypt('password'),'user_type'=>4,'faculty_id'=>1,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Guest_Mathematics','email'=>'guest_math@gmail.com','password'=>bcrypt('password'),'user_type'=>4,'faculty_id'=>2,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Guest_IT','email'=>'guest_it@gmail.com','password'=>bcrypt('password'),'user_type'=>4,'faculty_id'=>3,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')],
            ['name'=>'Guest_History','email'=>'guest_history@gmail.com','password'=>bcrypt('password'),'user_type'=>4,'faculty_id'=>4,'created_at'=> Carbon::now('Asia/Ho_Chi_Minh'),'updated_at'=> Carbon::now('Asia/Ho_Chi_Minh')]
        ]);
    }
}
