<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribution;
use Illuminate\Support\Facades\Auth;

class GuestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            abort_unless(Auth::user() -> isGuest(), 403);
            return $next($request);
        });
    }
    public function index(){
        $contributions = Contribution::where('faculty_id', Auth::user()->faculty_id)->where('status',2)->orderBy('id', 'DESC')->paginate(10);
        return view('guest.index',compact('contributions'));
    }
}
