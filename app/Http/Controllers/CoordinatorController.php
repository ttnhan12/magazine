<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Contribution;
use App\Faculty;

class CoordinatorController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            abort_unless(Auth::user() -> isMarketingCoordinator(), 403);
            return $next($request);
        });
    }
    public function index(){
        $contributions = Contribution::where('faculty_id', Auth::user()->faculty_id)->orderBy('id', 'DESC')->paginate(10);
        return view('coordinator.index',compact('contributions'));
    }

    public function comment(Request $request, $id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::user()->faculty_id != $contribution->faculty_id, 403);
        return view('coordinator.comment',compact('contribution'));
    }

    public function update(Request $request, $id){
        if(empty($request->comment)){
            return back()->withErrors(["You're commenting, please say something!"]);
        }

        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::user()->faculty_id != $contribution->faculty_id, 403);
        $contribution->comment = $request->comment;
        $contribution->comment_date = \Carbon\Carbon::now();
        $contribution->save();
        $request->session()->flash('status', 'Done!');
        return redirect()->route('coordinator.index');
    }

    public function request(Request $request, $id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::user()->faculty_id != $contribution->faculty_id, 403);
        $contribution->status = 1;
        $contribution->save();
        return redirect()->route('coordinator.index');
    }

    public function cancel(Request $request, $id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::user()->faculty_id != $contribution->faculty_id, 403);
        $contribution->status = 0;
        $contribution->save();
        $request->session()->flash('status', 'Done!');
        return redirect()->route('coordinator.index');
    }
}
