<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Contribution;
use App\Closure;
use Carbon\Carbon;
use Notification;
use App\Notifications\NewFile;

class StudentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            abort_unless(Auth::user() -> isStudent(), 403);
            return $next($request);
        });
    }
    public function index(){
        $student = Auth::user();
        $contributions = Contribution::where('user_id',$student->id)->orderBy('id','DESC')->paginate(10);
        return view('student.index',compact('contributions'));
    }
    public function create(){
        // $student = Auth::user();
        // $name = $student->name;
        // Notification::route('mail', 'chuongphong12@gmail.com')->notify(new NewFile($name));
        return view('student.create');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required',
            'file.*'   => 'mimes:png,jpeg,docx,doc,zip'
        ]);
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $path = Storage::putFile('contributions', $file);
        Storage::move($path,'public/'.$path);
        Contribution::create([
            'file_path'=>$path,
            'file_extension'=>$file->extension(),
            'file_name'=>$fileName,
            'user_id'=>Auth::id(),
            'faculty_id'=>Auth::user()->faculty_id
        ]);

        return response()->json(['success'=>'Upload successfully!']);
    }

    public function sendNoti() {
        $student = Auth::user();
        $name = $student->name;
        // Notification::route('mail', 'huypham1601@gmail.com')->notify(new NewFile($name));

        return redirect()->route('student.index')->with('status', 'Your contribution has been submiited. An email will send to your Faculty Coordinator!');
    }

    public function edit($id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::id() != $contribution->user_id, 403);
        return view('student.edit', compact('contribution'));
    }

    public function update(Request $request, $id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::id() != $contribution->user_id, 403);
        $contribution->description = $request->description;
        $contribution->save();
        $request->session()->flash('status', 'Update successfully!');
        return redirect()->route('student.index');
    }

    public function destroy($id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::id() != $contribution->user_id, 403);
        Storage::delete('public/'.$contribution->file_path);
        $contribution->delete();
        return redirect()->route('student.index');
    }

    public function accept($id){
        $contribution = Contribution::findOrFail($id);
        abort_if(Auth::id() != $contribution->user_id, 403);
        $contribution->status = 2;
        $contribution->save();
        return redirect()->route('student.index');
    }
}
