<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->user_type == 0){
            return redirect()->route('admin.index');
        }else if($user->user_type == 1){
            return redirect()->route('manager.index');
        }else if($user->user_type == 2){
            return redirect()->route('coordinator.index');
        }else if($user->user_type == 3){
            return redirect()->route('student.index');
        }else{
            return redirect()->route('guest.index');
        }
    }
}
