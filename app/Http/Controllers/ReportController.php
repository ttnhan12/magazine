<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Contribution;
use App\Faculty;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function contributions(){
        $current = Carbon::now()->year;
        $list = [$current-4,$current-3,$current-2,$current-1,$current];
        $philosophy = Contribution::where('faculty_id',1)->get();
        $mathematics = Contribution::where('faculty_id',2)->get();
        $inte = Contribution::where('faculty_id',3)->get();;
        $history = Contribution::where('faculty_id',4)->get();

        $ph=[0,0,0,0,0];
        foreach($philosophy as $item){
            $y = Carbon::parse($item->created_at)->year;
            switch ($y) {
                case $list[0]:
                    $ph[0]++;
                    break;
                case $list[1]:
                    $ph[1]++;
                    break;
                case $list[2]:
                    $ph[2]++;
                    break;
                case $list[3]:
                    $ph[3]++;
                    break;
                case $list[4]:
                    $ph[4]++;
                    break;
                default:
                    break;
            }
        }
        $ma=[0,0,0,0,0];
        foreach($mathematics as $item){
            $y = Carbon::parse($item->created_at)->year;
            switch ($y) {
                case $list[0]:
                    $ma[0]++;
                    break;
                case $list[1]:
                    $ma[1]++;
                    break;
                case $list[2]:
                    $ma[2]++;
                    break;
                case $list[3]:
                    $ma[3]++;
                    break;
                case $list[4]:
                    $ma[4]++;
                    break;
                default:
                    break;
            }
        }
        $it=[0,0,0,0,0];
        foreach($inte as $item){
            $y = Carbon::parse($item->created_at)->year;
            switch ($y) {
                case $list[0]:
                    $it[0]++;
                    break;
                case $list[1]:
                    $it[1]++;
                    break;
                case $list[2]:
                    $it[2]++;
                    break;
                case $list[3]:
                    $it[3]++;
                    break;
                case $list[4]:
                    $it[4]++;
                    break;
                default:
                    break;
            }
        }
        $hi=[0,0,0,0,0];
        foreach($history as $item){
            $y = Carbon::parse($item->created_at)->year;
            switch ($y) {
                case $list[0]:
                    $hi[0]++;
                    break;
                case $list[1]:
                    $hi[1]++;
                    break;
                case $list[2]:
                    $hi[2]++;
                    break;
                case $list[3]:
                    $hi[3]++;
                    break;
                case $list[4]:
                    $hi[4]++;
                    break;
                default:
                    break;
            }
        }    
        return view('report.number_contributions',[
            'ph' => json_encode($ph),
            'ma' => json_encode($ma),
            'it' => json_encode($it),
            'hi' => json_encode($hi),
            'list' => json_encode($list)
        ]);
    }
    public function contributors(){
        $current = Carbon::now()->year;
        // 2015, 2016, 2017, 2018, 2019
        $list = [$current-4,$current-3,$current-2,$current-1,$current];

        $phi = collect([]);
        foreach($list as $item){
            $philosophy = Contribution::where('faculty_id',1)->whereYear('created_at',$item)->get();
            $counted = $philosophy->countBy(function($contribution){
                return $contribution->user_id;
            })->count();
            $phi->push($counted);
        }
        $ph = Collection::unwrap($phi);
        
        $mat = collect([]);
        foreach($list as $item){
            $mathematics = Contribution::where('faculty_id',2)->whereYear('created_at',$item)->get();
            $counted = $mathematics->countBy(function($contribution){
                return $contribution->user_id;
            })->count();
            $mat->push($counted);
        }
        $ma = Collection::unwrap($mat);

        $intec = collect([]);
        foreach($list as $item){
            $informatech = Contribution::where('faculty_id',3)->whereYear('created_at',$item)->get();
            $counted = $informatech->countBy(function($contribution){
                return $contribution->user_id;
            })->count();
            $intec->push($counted);
        }
        $it = Collection::unwrap($intec);

        $his = collect([]);
        foreach($list as $item){
            $history = Contribution::where('faculty_id',4)->whereYear('created_at',$item)->get();
            $counted = $history->countBy(function($contribution){
                return $contribution->user_id;
            })->count();
            $his->push($counted);
        }
        $hi = Collection::unwrap($his);

        return view('report.number_contributors',[
            'ph' => json_encode($ph),
            'ma' => json_encode($ma),
            'it' => json_encode($it),
            'hi' => json_encode($hi),
            'list' => json_encode($list)
        ]);
    }
    private function chartData($year){
        $faculties = Faculty::all();
        if ( $year == 9999 ){
            $total = Contribution::all()->count();
            $data = $faculties->map(function($faculty, $key) use ($year){
                return $faculty->contributions->count();
            });
            $labels = $faculties->map(function ($faculty, $key){
                return $faculty->name;
            });
            $combined = $labels->combine($data);
            if($total != 0){
                $percentages = $combined->map(function($item, $key) use ($total){
                    return round($item/$total*100,2);
                });
                return ["data"=>$data, "labels"=>$labels,"percentages"=>$percentages];
            }else{
                $percentages = $combined->map(function($item, $key) use ($total){
                    return 0;
                });
                return ["data"=>$data, "labels"=>$labels,"percentages"=>$percentages];
            }
        } else {
            $contributions = DB::table('contributions')->whereYear('created_at', $year)->get();
            $total = $contributions->count();
            $data = $faculties->map(function($faculty, $key) use ($year){
                $collection = collect($faculty->contributions);
                $filtered = $collection->filter(function ($item, $key) use ($year){ 
                    return Carbon::parse($item->created_at)->year == $year;
                });
                return $filtered->count();
            });
            $labels = $faculties->map(function ($faculty, $key){
                return $faculty->name;
            });
            $combined = $labels->combine($data);
            if($total != 0){
                $percentages = $combined->map(function($item, $key) use ($total){
                    return round($item/$total*100,2);
                });
                return ["data"=>$data, "labels"=>$labels,"percentages"=>$percentages];
            }else{
                $percentages = $combined->map(function($item, $key) use ($total){
                    return 0;
                });
                return ["data"=>$data, "labels"=>$labels,"percentages"=>$percentages];
            }
        }
       
    }
    public function percentage(){
        $things = $this->chartData(9999);
        return view('report.percentage_contributions',[
            "data" => $things["data"],
            "labels" =>$things["labels"],
            "percentages" => $things["percentages"],
            "current"=> 9999
        ]);
    }
    public function updatePercentage(Request $request){
        $year = $request->year;
        $things = $this->chartData($year);
        return view('report.percentage_contributions',[
            "data" => $things["data"],
            "labels" =>$things["labels"],
            "percentages" => $things["percentages"],
            "current"=> $year
        ]);
    }
    public function withoutConment(){
        $contributions = Contribution::where('comment', null)->paginate(10);
        return view('report.without_comment', compact('contributions'));
    }
    public function afterFourteen(){ //this report is the report for the late comment
        $contributions = Contribution::all()->filter(function($contribution, $key){
            if($contribution->comment != null){
                $create_date = Carbon::parse($contribution->created_at);
                $comment_date = Carbon::parse($contribution->comment_date);
                return $comment_date->diffInDays($create_date) > 14;
            }
        });
        return view('report.without_comment_fourteen',compact('contributions'));
    }
}
